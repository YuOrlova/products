import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    products:[
      {
        id:"12",
        title:"Наименование товара",
        src: "https://btest.ru/images/Fotki/VideoKamery/Sony/DCRSD1000E/sony_dcr-sd1000e.jpg",
        description: "Довольно-таки интересное описание товара в несколько строк. Довольно-таки интересное описание товара в несколько строк",
        price: "10 000",
      },
      {
        id:"14",
        title:"Наименование товара",
        src: "https://btest.ru/images/Fotki/VideoKamery/Sony/DCRSD1000E/sony_dcr-sd1000e.jpg",
        description: "Довольно-таки интересное описание товара в несколько строк. Довольно-таки интересное описание товара в несколько строк",
        price: "10 000",
      },
      {
        id:"16",
        title:"Наименование товара",
        src: "https://btest.ru/images/Fotki/VideoKamery/Sony/DCRSD1000E/sony_dcr-sd1000e.jpg",
        description: "Довольно-таки интересное описание товара в несколько строк. Довольно-таки интересное описание товара в несколько строк",
        price: "10 000",
      },
      {
        id:"19",
        title:"Наименование товара",
        src: "https://btest.ru/images/Fotki/VideoKamery/Sony/DCRSD1000E/sony_dcr-sd1000e.jpg",
        description: "Довольно-таки интересное описание товара в несколько строк. Довольно-таки интересное описание товара в несколько строк",
        price: "10 000",
      },
      {
        id:"10",
        title:"Наименование товара",
        src: "https://btest.ru/images/Fotki/VideoKamery/Sony/DCRSD1000E/sony_dcr-sd1000e.jpg",
        description: "Довольно-таки интересное описание товара в несколько строк. Довольно-таки интересное описание товара в несколько строк",
        price: "10 000",
      },
      {
        id:"17",
        title:"Наименование товара",
        src: "https://btest.ru/images/Fotki/VideoKamery/Sony/DCRSD1000E/sony_dcr-sd1000e.jpg",
        description: "Довольно-таки интересное описание товара в несколько строк. Довольно-таки интересное описание товара в несколько строк",
        price: "10 000",
      },
  ],
  validInput: false,
  },
  getters: {
    GET_PRODUCTS(state) {
      return state.products;
    },
    VALID_INPUT(state) {
      return state.validInput;
    },
  },
  mutations: {
    SET_ADD_PRODUCTS(state,form){
      state.products.push(form)
    },
    SET_DELETE_PRODUCTS(state,title){
      let i
      for (i= 0; i< state.products.length;i++){
        console.log(state.products[i].id)
        console.log(title)
        console.log(title==state.products[i].id)
        if(title==state.products[i].id){
          state.products.splice(i,1)
        }
      }
    },
    UPDATE_VALID_INPUT(state, validity) {
      state.validInput = validity;
    },
  },
  actions: {
    addProduct(context,form){
      context.commit("SET_ADD_PRODUCTS",form);
    },
    deleteProduct(context,title){
      context.commit("SET_DELETE_PRODUCTS",title);
      
    },
    updateInputValidity(context, validity) {
      context.commit("UPDATE_VALID_INPUT", validity);
    },
  },
  modules: {},
});
